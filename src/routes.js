import VueRouter from 'vue-router'
import Home from './components/Home'
import Login from './components/Login'
import Users from './components/User/Users'
import Courses from './components/Course/CoursesList'
import Chat from './components/Chat'
import WorkInProgress from './components/Utils/WorkInProgress'

var routes = [
{ path: '/', component: Home},
{ path: '/login', component: Login},
{ path: '/users', component: Users},
{ path: '/courses', component: Courses},
{ path: '/wip', component: WorkInProgress},
{ path: '/chat', component: Chat}
]

var router = new VueRouter({
	routes
})

export default router
