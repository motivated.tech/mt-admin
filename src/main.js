import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import io from 'socket.io-client'
import App from './App'

Vue.use(VueRouter)
// Vue.use(VueResource)

Vue.prototype.$http = axios
Vue.prototype.$io = io

import router from './routes'

new Vue({
	router,
	el: '#app',
	render: (h) => h(App)
})
