import auth from './auth'
import backend from './backend'
const API_URL = backend.API_URL

export default {
	getChat(context, id) {
		return new Promise((resolve, reject) => {
			context.$http.get(`${API_URL}/chat/${id}`, {headers: { Authorization: auth.getBearer() }})
			.catch((err) => console.error(err))
			.then((res) => resolve(res.data))
		})
	}
}