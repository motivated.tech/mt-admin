import backend from './backend'
const API_URL = backend.API_URL
const LOGIN_URL = API_URL + '/token'


export default {
	user: {
		authenticated: false
	},

  login(context, creds, redir, callback) {  	
  	context.$http.post(LOGIN_URL, creds).then((res) => {
      if(res.data.id_token) {
        localStorage.setItem('id_token', res.data.id_token)
        this.user.authenticated = true
        context.$router.push(redir)
      }
    }).catch((err) => {
      return callback(err)
    })
  },

  logout() {
  	localStorage.removeItem('id_token')
  	this.user.authenticated = false
  },

  checkAuth() {
  	var jwt = localStorage.getItem('id_token')
  	if(jwt) {
  		this.user.authenticated = true
  	}
  	else {
  		this.user.authenticated = false      
  	}
  },

  getBearer() {
  	return 'Bearer ' + localStorage.getItem('id_token')
  },
  
   getAuthHeader() {
    return {
      'Authorization': 'Bearer ' + localStorage.getItem('id_token')
    }
  }

}