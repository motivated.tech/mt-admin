import auth from './auth'
import backend from './backend'
const API_URL = backend.API_URL

export default {
	getAllCourses(context) {
		return new Promise((resolve, reject) => {
			context.$http.get(`${API_URL}/course`, {headers: { Authorization: auth.getBearer() }})
			.catch((err) => console.error(err))
			.then((res) => resolve(res.data))
		})
	},

	newCourse(context, user) {
		return new Promise((resolve, reject) => {
			context.$http.post(`${API_URL}/course`, user, {headers: auth.getAuthHeader()})
			.catch((err) => console.error(err))
			.then((res) => resolve(res.data))
		})
	},

	deleteUser(context, id) {
		context.$http.delete(`${API_URL}/course/${id}`, {headers: {Authorization: auth.getBearer()}})
		.catch((err) => console.error(err))
		// .then((res) => {console.log(res)})
	}
}