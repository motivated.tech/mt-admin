import auth from './auth'
import backend from './backend'
const API_URL = backend.API_URL

export default {
	getAllUsers(context) {
		return new Promise((resolve, reject) => {
			context.$http.get(`${API_URL}/user/`, {headers: { Authorization: auth.getBearer() }})
			.catch((err) => console.error(err))
			.then((res) => resolve(res.data))
		})
	},

	newUser(context, user) {
		return new Promise((resolve, reject) => {
			context.$http.post(`${API_URL}/user/`, user, {headers: auth.getAuthHeader()})
			.catch((err) => console.error(err))
			.then((res) => resolve(res.data))
		})
	},

	deleteUser(context, id) {
		return new Promise((resolve, reject) => {
			context.$http.delete(`${API_URL}/user/${id}`, {headers: {Authorization: auth.getBearer()}})
			.catch((err) => console.error(err))
			.then((res) => resolve(res.data))
		})
	}
	
}